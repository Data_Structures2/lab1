public class Lab1_3 {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1; // Index for nums1
        int j = n - 1; // Index for nums2
        int mergedIndex = m + n - 1; // Index for the merged array

        // Merge the arrays from the back
        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[mergedIndex] = nums1[i];
                i--;
            } else {
                nums1[mergedIndex] = nums2[j];
                j--;
            }
            mergedIndex--;
        }

        // Copy any remaining elements from nums2 to nums1
        while (j >= 0) {
            nums1[mergedIndex] = nums2[j];
            j--;
            mergedIndex--;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0}; // The "0" values represent empty spaces
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;

        merge(nums1, m, nums2, n);

        System.out.print("Merged array: ");
        for (int num : nums1) {
            System.out.print(num + " ");
        }
    }
}

public class Lab1 {
    public static void main(String[] args) {
        // Declare and initialize the arrays
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        // Print elements of the "numbers" array using a for loop
        System.out.print("Numbers array: ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        // Print elements of the "names" array using a for-each loop
        System.out.print("Names array: ");
        for (String name : names) {
            System.out.print(name + " ");
        }
        System.out.println();

        // Initialize the elements of the "values" array
        values[0] = 2.5;
        values[1] = 4.7;
        values[2] = 1.3;
        values[3] = 3.9;

        // Calculate and print the sum of all elements in the "numbers" array
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println("Sum of numbers: " + sum);

        // Find and print the maximum value in the "values" array
        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("Maximum value in values: " + max);

        // Create a new array to store reversed names
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        // Print the elements of the "reversedNames" array
        System.out.print("Reversed names array: ");
        for (String reversedName : reversedNames) {
            System.out.print(reversedName + " ");
        }
        System.out.println();
    }
}


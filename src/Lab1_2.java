public class Lab1_2 {
    public static void duplicateZeros(int[] arr) {
        int n = arr.length;
        int zerosCount = 0;

        // Count the number of zeros in the original array
        for (int num : arr) {
            if (num == 0) {
                zerosCount++;
            }
        }

        // Initialize pointers for the original and modified arrays
        int originalIndex = n - 1;
        int modifiedIndex = n - 1 + zerosCount;

        // Handle zero duplication and shifting
        while (originalIndex >= 0) {
            if (modifiedIndex < n) {
                arr[modifiedIndex] = arr[originalIndex];
            }

            if (arr[originalIndex] == 0) {
                modifiedIndex--;
                if (modifiedIndex < n) {
                    arr[modifiedIndex] = 0;
                }
            }

            originalIndex--;
            modifiedIndex--;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 0, 2, 3, 0, 4, 5, 0};
        System.out.print("Original array: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();

        duplicateZeros(arr);

        System.out.print("Modified array: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }
}
